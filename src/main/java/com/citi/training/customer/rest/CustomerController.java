package com.citi.training.customer.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.customer.dao.CustomerDao;
import com.citi.training.customer.model.Customer;
import com.citi.training.customer.rest.CustomerController;

@RestController
@RequestMapping("/customer")
public class CustomerController {

	private static Logger LOG = 
			LoggerFactory.getLogger(CustomerController.class);
	
	@Autowired
	private CustomerDao customerdao;
	
	@RequestMapping(method=RequestMethod.GET)
	public List<Customer> getAllCustomers(){
		LOG.info("getAllCustomers was called");
		LOG.debug("This is a debug message");
		return customerdao.getAllCustomers();
	}
	
	@RequestMapping(method=RequestMethod.POST,
			produces=MediaType.APPLICATION_JSON_VALUE,
			consumes=MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Customer> save (@RequestBody Customer customer){
		customerdao.saveCustomer(customer);
		LOG.info("Create a customer");
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}
	
	@RequestMapping(value="/{id}",
					method=RequestMethod.GET,
					produces=MediaType.APPLICATION_JSON_VALUE)
	public Customer get(@PathVariable int id) {
		LOG.info("get one Customer was called");
		return customerdao.getCustomer(id);
	}
	
	@RequestMapping(value="/{id}",
			method=RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable int id) {
		LOG.info("Removing Customer id: " + id );
		customerdao.deleteCustomer(id);
	}
	
}
