package com.citi.training.customer.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.citi.training.customer.dao.CustomerDao;
import com.citi.training.customer.model.Customer;

@Component
public class Demo implements ApplicationRunner {
	
	@Autowired
    private CustomerDao customerRepository;

    public void run(ApplicationArguments appArgs) {
        System.out.println("Creating some customer");
        String[] customerNames = {"Karen", "Daniel", "Clare"};
        String[] customerAddresses = {"123 Street", "456 Avenue", "789 Lane"};

        for(int i=0; i<customerNames.length; ++i) {
            Customer thisCustomer = new Customer(i,
                                              customerNames[i],
                                              customerAddresses[i]);

            System.out.println("Created Customer: " + thisCustomer);

            customerRepository.saveCustomer(thisCustomer);
        }

        System.out.println("\nAll Customers:");
        for(Customer customer: customerRepository.getAllCustomers()) {
            System.out.println(customer);
        }
  
        System.out.println("Demo Finished - Exiting");
    }

}
