package com.citi.training.customer.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.citi.training.customer.exceptions.CustomerNotFoundException;
import com.citi.training.customer.model.Customer;

@Component
public class InMemCustomerDao implements CustomerDao {

	private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();

	public void saveCustomer(Customer customer) {
        allCustomers.put(customer.getId(), customer);
    }

    public Customer getCustomer(int id) {
    	if (allCustomers.containsKey(id)) {
    		return allCustomers.get(id);
        } else {
        	throw new CustomerNotFoundException("customer not found");
        }
    }

    public List<Customer> getAllCustomers() {
        return new ArrayList<Customer>(allCustomers.values());
    }
    

    public void deleteCustomer(int id) {
    	Customer removeCustomer = allCustomers.remove(id);
    	if (removeCustomer == null) {
    		throw new CustomerNotFoundException("Customer was not found : " + id);
    	}
    }

}
