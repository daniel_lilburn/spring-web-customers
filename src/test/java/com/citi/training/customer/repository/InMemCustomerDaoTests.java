package com.citi.training.customer.repository;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.citi.training.customer.dao.InMemCustomerDao;
import com.citi.training.customer.exceptions.CustomerNotFoundException;
import com.citi.training.customer.model.Customer;

public class InMemCustomerDaoTests {

    private static Logger LOG = LoggerFactory.getLogger(InMemCustomerDaoTests.class);

    private int id = 19;
    private String name = "A customer for testing!";
    private String address = "Johnston Road";

    @Test
    public void test_saveCustomer() {
        Customer testcustomer = new Customer(id, name, address);
        InMemCustomerDao testRepo = new InMemCustomerDao();

        testRepo.saveCustomer(testcustomer);
 
        assertTrue(testRepo.getCustomer(id).equals(testcustomer));

    }

    @Test
    public void test_getAllCustomers() {
        Customer[] testcustomerArray = new Customer[100];
        InMemCustomerDao testRepo = new InMemCustomerDao();

        for(int i=0; i<testcustomerArray.length; ++i) {
            testcustomerArray[i] = new Customer(id + i, name, address + i);

            testRepo.saveCustomer(testcustomerArray[i]);
        }

        List<Customer> returnedcustomers = testRepo.getAllCustomers();
        LOG.info("Received [" + returnedcustomers.size() +
                 "] customers from repository");

        for(Customer thiscustomer: testcustomerArray) {
            assertTrue(returnedcustomers.contains(thiscustomer));
        }
        LOG.info("Matched [" + testcustomerArray.length + "] customers");
    }
    
    @Test(expected = CustomerNotFoundException.class)
    public void test_getcustomerNotFound() {
    	
    	InMemCustomerDao testRepo = new InMemCustomerDao();
    	int invalidId = 7;
    	
    	testRepo.getCustomer(99);
    	
    }
    
    @Test(expected = CustomerNotFoundException.class)
    public void test_removecustomer() {
    	
    	InMemCustomerDao testRepo = new InMemCustomerDao();
    	int invalidId = 7;
    	
    	testRepo.deleteCustomer(99);
    }
}

