package com.citi.training.customer.model;

import static org.junit.Assert.*;

import org.junit.Test;


public class CustomerUnitTests {
	
    private int id = 5;
    private String name = "John Doe";
    private String address = "5 Hillside Park";

    @Test
    public void test_Customer_constructor() {
        Customer testCustomer = new Customer(id, name, address);;

        assertEquals(id, testCustomer.getId());
        assertEquals(name, testCustomer.getName());
        assertEquals(address, testCustomer.getAddress());
    }

    @Test
    public void test_Customer_toString() {
        String testString = new Customer(id, name, address).toString();

        assertTrue(testString.contains((new Integer(id)).toString()));
        assertTrue(testString.contains(name));
        assertTrue(testString.contains(address));
    }
}